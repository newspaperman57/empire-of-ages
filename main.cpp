/*

MAIN:
What does main usually do?

*/

#include <iostream>
#include <SDL2/SDL.h>
#include "Headers/ENG_Game.h"
#include "Headers/ENG_Eventhandler.h"
#include "Headers/ENG_Config.h"
#include "Headers/GS_Game.h"

using namespace std;

Game game; // Initializes game class globally, so accessible to all classes

/*
Lol do you really need a description?
@param args The total amount of arguments given.
@param arg[] The actual arguments given
@return int To notify the OS of absolutely nothing...
*/

int main(int args, char* arg[])
{
	cout << "Main Function!" << endl;
	SDL_Renderer* renderer = game.Initialize(WWIDTH, WHEIGHT,(char*)"Empire of Ages" , SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED); // Initializes SDL and creates a window. 
	
	GS_Game gs_game;
	gs_game.Initialize(renderer);
	cout << "Registering functions!" << endl;
	
	game.SetConstructFunction(GAME, GS_Game::Constructor);
	game.SetLoopFunction(GAME, GS_Game::Loop);
	game.SetRenderFunction(GAME, GS_Game::Render);
	game.SetDestructFunction(GAME, GS_Game::Destructor);
	
	game.Run( 0 ); // Runs mainloop. Will continue until game.Quit() is called.
	game.Close(); // Closes window. Opposite of Game::Initialize()
	
	cout << "Reached end of program!" << endl;
	return 0;
}
