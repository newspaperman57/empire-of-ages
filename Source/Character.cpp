/*
Character rendering and animation

*/

#include "../Headers/Character.h"

void Character::Initialize(int x, int y, const char* path, SDL_Renderer* rend, int gridWidth, int gridHeight )
{
	cout << "Initializing Character!" << endl;
	pos.x = x;
	pos.y = y;
	speed = 2;
	center = new SDL_Point;
	center->x = gridHeight;
	center->y = gridWidth;
	state = STANDING;
	animationCount = 0;
	sprites.Initialize( path, rend, gridWidth, gridHeight);
	sprites.SetScale(2);
}

void Character::Initialize(SDL_Rect newPos, const char* path, SDL_Renderer* rend, int gridWidth, int gridHeight)
{
	Initialize(newPos.x, newPos.y, path, rend, gridWidth, gridHeight);
}

SDL_Rect Character::GetPos()
{
	SDL_Rect temp;
	temp.x = pos.x;
	temp.y = pos.y;
	return temp;
}

void Character::Render(SDL_Renderer* renderer)
{
	animationCount++;
	switch(state)
	{
		case WALKING:
			//cout << (int)(animationCount/(4.0/speed))%3 << "  " << 0 << "  " << pos.x << "  " << pos.y << "  " << renderer << endl;
			sprites.Render(1+(int)(animationCount/(10.0/speed))%2, walkingDirection,pos.x,pos.y,renderer);
			break;
		case STANDING:
			sprites.Render(0, 0,pos.x,pos.y,renderer);
			break;
	}
	
}

void Character::Loop()
{
	angle = 90+(std::atan2(unitVector.y, unitVector.x) / M_PI) * 180.0;
	angle += 90;
	if(angle > 315 || angle <= 45) walkingDirection = RIGHT;
	else if(angle > 225) walkingDirection = UP;
	else if(angle > 135) walkingDirection = LEFT;
	else if(angle > 45) walkingDirection = DOWN;
	else cout << "ERROR! Direction wasn't found! Angle: " << angle << endl;
	walkedDistance += LengthOfVector(unitVector.x, unitVector.y);
	if(magnitude/2 > walkedDistance)
	{
		pos.x += unitVector.x*speed;
		pos.y += unitVector.y*speed;
	} else state = STANDING;
}

double Character::LengthOfVector(double x, double y)
{
	if((x*x+y*y) == 0)
		return 0;
	return sqrt(abs(x*x+y*y));
}

void Character::FindUnitVector()
{
	magnitude = LengthOfVector(pos.x+center->x-goal.x, pos.y+center->x-goal.y);
	if(magnitude != 0)
	{
		unitVector.x = -(pos.x+center->x-goal.x)/magnitude;
		unitVector.y = -(pos.y+center->y-goal.y)/magnitude;
		state = WALKING;
	} else {
		cout << "Already at Goal!" << endl;
		state = STANDING;
	}
}

void Character::SetPos(SDL_Rect newPos)
{
	SetPos(newPos.x, newPos.y);
}

void Character::SetPos(int x, int y)
{
	pos.x = x;
	pos.y = y;
}

void Character::WorkAt(int x, int y)
{
	walkedDistance = 0;
	if( !(goal.x == pos.x && goal.y == pos.y) )
	{
		goal.x = x;
		goal.y = y;
		FindUnitVector();
	}
}
