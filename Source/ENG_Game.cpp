/*
Sourcefile for 'Headers/Game.h'
Source file for class 'Game'

Runs Mainloop
*/

#include "../Headers/ENG_Game.h"


bool Game::quit;
/*
Constructor for class 'Game'. Initializes all variables in the class
@param args Number of arguments provided to program. Directly or modified from main function
@param arg Arguments provided to program. Directly or modified from main function
@return no return value (Constructor)
*/
Game::Game()
{
	window = NULL;
	renderer = NULL;
	texture = NULL;
	test_Surface = NULL;
	
	gameState = -1;
	quit = false;
	closed = true;
}

/*
Destructor for class 'Game'
@return no return value (Destructor)
*/
Game::~Game()
{
	if(!closed) Close();
}

/*
Initializes SDL and Window. Loads graphics
@param windowWidth The window Width.
@param windowHeight The window Height.
@param title The title of the created window
@param windowPositionX The X position of the window on the screen
@param windowPositionY The Y position of the window on the screen
@return bool True if succesfull, false if otherwise
*/
SDL_Renderer* Game::Initialize(int windowWidth, int windowHeight, char* title, int windowPositionX, int windowPositionY)
{
	cout << "Game::Initialize() Started" << endl;
	cout << "Initializing SDL";
	if( SDL_Init( SDL_INIT_EVERYTHING ) < 0 ) cout << "  ERROR: " << SDL_GetError() << endl;
	else cout << " OK!" << endl;
	
	cout << "Initializing SDL_Image";
	if ((IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG) != IMG_INIT_PNG) cout << "  ERROR: " << SDL_GetError();
	else cout << " OK!" << endl;
	
	cout << "Initializing Window";
	if( (window = SDL_CreateWindow(title, windowPositionX, windowPositionY, windowWidth, windowHeight, SDL_WINDOW_SHOWN) ) == NULL ) cout << "  ERROR: " << SDL_GetError() << endl;
	else cout << " OK!" << endl;
	
	cout << "Initializing Renderer";
	if( (renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC) ) == NULL) cout << "  ERROR: " << SDL_GetError() << endl;
	else cout << " OK!" << endl;
	
	cout << "Initializing Eventhandler";
	if( Eventhandler::Initialize() ) cout << " ERROR!" << endl;
	else cout << " OK!" << endl;
	
	cout << "INITIALIZATION COMPLETE!" << endl;
	cout << "Loading Graphics:" << endl;
	
	cout << "Done loading Graphics" << endl;
	
	Eventhandler::Register(-1, SDL_QUIT, &Game::OnQuitEvent ); // Register shutdown events in eventhandler
	Eventhandler::Register(-1, SDL_KEYDOWN, &Game::OnQuitEvent ); // Escape shuts down the game too
	
	cout << "Game::Initialize() Ended" << endl;
	closed = false;
	return renderer;
}

/*
Runs the most of the program. Mainloop is located here
@param startState The state the program should begin with (Menu or Game)
@return bool True on succesfull run. False otherwise
*/
bool Game::Run(int startState)
{
	cout << "Game::Run() Started!" << endl;
	if(closed) // If closed is true, we are not ready for playing. Initialize first! 
	{
		cout << "ERROR: Initialize the goddamn class first! ( Game::Initialize() )" << endl;
		return false;
	}
	
	gameState = startState;
	if(!ChangeGamestate(gameState)) return false;
	ConstructFunction[gameState](); // Prepare gamestate for running
		
	cout << "MAINLOOP!" << endl;
	while(!quit)
	{
		SDL_RenderClear(renderer); // Clear screen to avoid ghosting
	
		Eventhandler::ProcessEvents( gameState ); // Process events
		
		LoopFunction[gameState](); // Everything not related to events or rendering. f.ex. Collisionchecking or AI
		RenderFunction[gameState]( renderer ); // Everything related to rendering. Putting shit on screen
		
		if(gameState != newGameState) // If we are changing gamestate, do that
		{
			DestructFunction[gameState](); // Destruct and inform old gamestate, that it will no longer be runned. 
			ConstructFunction[newGameState](); // Prepare new gamestate for running
			gameState = newGameState; // Change gamestate
		}
		// Wait for VSYNC and "Flip"
		SDL_RenderPresent(renderer);
	}
	DestructFunction[gameState](); // Close down gamestate
	
	gameState = -1; // Checkbit
	newGameState = -1; // Checkbit
	cout << "Game::Run() Ended!" << endl;
	return true;
}

/*
Change gamestate at end of this loop. Check if that is acceptable.
@param newGameState The new gamestate
@return bool True on succes, false otherwise
*/
bool Game::ChangeGamestate(int newGameState)
{
	cout << "Changing gamestate!" << endl;
	// Check for missing functions
	if(ConstructFunction[newGameState] == NULL)
	{
		cout << "ERROR WHEN CHANGING GAMESTATE: No ConstructFunction for gamestate " << newGameState << endl;
		return false;
	}
	if(DestructFunction[newGameState] == NULL)
	{
		cout << "ERROR WHEN CHANGING GAMESTATE: No DestructFunction for gamestate " << newGameState << endl;
		return false;
	}
	if(LoopFunction[newGameState] == NULL)
	{
		cout << "ERROR WHEN CHANGING GAMESTATE: No LoopFunction for gamestate " << newGameState << endl;
		return false;
	}
	if(RenderFunction[newGameState] == NULL)
	{
		cout << "ERROR WHEN CHANGING GAMESTATE: No RenderFunction for gamestate " << newGameState << endl;
		return false;
	}
	this->newGameState = newGameState; // Save the new gamestate, for changing at end of loop
	return true;
}

/*
Sets contructfunction for a particular gamestate
@param gamestate The gamestate that the function constructs
@param function The function that constructs the gamestate
*/
void Game::SetConstructFunction( int gamestate, void (*function)() )
{
	ConstructFunction[gamestate] = function;
}

/*
Sets loopFunction for a particular gamestate
@param gamestate The loop functions gamestate
@param function The gamestates loop function
*/
void Game::SetLoopFunction( int gamestate, void (*function)() )
{
	LoopFunction[gamestate] = function;
}

/*
Sets renderFunction for a particular gamestate
@param gamestate The renderFunctions gamestate
@param function The gamestates renderFunction. Needs to have SDL_Renderer* as parameter
*/
void Game::SetRenderFunction( int gamestate, void (*function)(SDL_Renderer*) )
{
	RenderFunction[gamestate] = function;
}

/*
Sets destructfunction for a particular gamestate
@param gamestate The gamestate that the function destructs
@param function The function that destructs the gamestate
*/
void Game::SetDestructFunction( int gamestate, void (*function)() )
{
	DestructFunction[gamestate] = function;
}

/*
Opposite of Game::Initialize()
Closes window and quits SDL.
@return bool True on succes. False otherwise
*/
bool Game::Close()
{
	if(!quit) // If the mainloop is still running, we want it to be ended before closing game
	{
		cout << "ERROR: Attempted to close game without quitting mainloop first( Game::Quit() )! NOT CLOSING!" << endl;
		return false;
	}
	SDL_DestroyTexture(texture);
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	IMG_Quit();
	SDL_Quit();
	closed = true;
	return true;
}

void Game::Quit()
{
	if(quit) cout << "ERROR: Attempted to quit game when already quitted!" << endl;
	quit == true;
}

/*
Will be runned by Eventhandler on Keydown or Quit events.
If the Escape key is pressed or a SDL_Quit event occured, quit will be set to true and the program will exit. 
@param event The event (Either KEYDOWN or SDL_QUIT) that just occurred
*/
static void Game::OnQuitEvent(SDL_Event event)
{
	if((event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_ESCAPE) || event.type == SDL_QUIT) 
	{
		cout << "QUIT event!" << endl;
		quit = true;
	}
}
