/*
Sourcefile for 'Headers/Render.h'
Sourcefile for class 'Render'

Have functions for everything connected to rendering
Loading textures from HDD
Makes it easier to get information about textures.
ALL STATIC!
NO NEED TO INSTANTIATE!
*/

#include "../Headers/ENG_Render.h"

static SDL_Rect Render::GetDimension(SDL_Texture* texture)
{
	SDL_Rect size;
	SDL_QueryTexture(texture, NULL, NULL, &size.w, &size.h);
	return size;
}

/*
Loads a Texture from the HDD
@param file The path to the file that needs to be loaded
@param renderer I have no idea...
@return SDL_Texture* The loaded texture or NULL if an error occured
*/
static SDL_Texture* Render::Load(char* file, SDL_Renderer *renderer)
{
	SDL_Texture *texture = IMG_LoadTexture(renderer, file);
	if (texture == NULL)
		cout << "Render::Load(): ERROR: " << SDL_GetError() << endl;
	return texture;
}
