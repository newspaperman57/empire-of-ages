/*

Handles all events. Main part of mainloop.
Runs appropiate functions when events happen. 

DEVELOP SYSTEM FOR OTHER CLASSES TO "HANG ON" TO A SPECIFIC EVENT
Like "Eventhandler::Register(eventType, Function)"
EventType is the event the function wants to be registered to.
When a event of that type happens, Function will be run, with the event as parameter 

*/

#include "../Headers/ENG_Eventhandler.h"

/*
Static members of eventhandler is initialized here.
*/
static SDL_Event Eventhandler::event;
static std::vector<registeredFunction> Eventhandler::functionRegister;

/*
Run before using any othere functions in this class.
Initializes functionRegister vector.
@return bool false on succes. True otherwise
*/
static bool Eventhandler::Initialize()
{
	Eventhandler eventhandler;
	return false;
}
/*
Registers a function and an eventType. The function will then be run when an event with that type gets polled

@param eventType The type of event the function will be run when polled
@param function The function which will be run when the event with type 'eventType' gets polled. The function gets runned with the event as parameter
*/
static void Eventhandler::Register(int gameState, int eventType, void (*function)(SDL_Event))
{
	functionRegister.push_back((registeredFunction){function, eventType, gameState});
}

/*
Processes all events. 
If a event type is equal to a registered type, the registered function will be run with the event as parameter
@param gameState The current gamestate. All the events registered with -1 or that gamestate will be processed.
*/
static void Eventhandler::ProcessEvents(int gameState)
{
	while (SDL_PollEvent(&event))
	{
		for(int i = 0; i < functionRegister.size(); i++)
		{
			if(functionRegister[i].eventType == event.type && (functionRegister[i].gameState == gameState || functionRegister[i].gameState == -1) )
			{
				functionRegister[i].function(event);
			}
		}
	}
}
