#include "../Headers/Map.h"

/*

Map is controlled by GS_Game
Map loads mapfile in Initialize
Map loads textures defined in mapfile
Renders textures in Renderfunction at positions defined in mapfile
Map has intern positions of where in the map it is. Render ALL map every time.
Position of map is minussed from position of textures in map 
if texture has pos 100, and pos of map is 100 the texture will be at 0
Map is also responsible for collisionchecking, which is loaded from 
mapfile as well.

*/

void Map::Initialize(char* path, SDL_Renderer* renderer)
{
	tileSize = 32;
	pos.x = 0;
	pos.y = 0;
	
	string temp, mapName;
	int temp1, temp2;
	
	ifstream file;
	
	file.open(path);
	file >> temp;
	if(temp != "\"")
		cout << "Mapfile invalid!" << endl;
	file >> temp;
	while(temp != "\"")
	{
		mapName += temp;
		file >> temp;
	}
	file >> gridSizeX;
	file >> gridSizeY;
	file >> temp1;
	for(int i = 0; i < temp1; i++)
	{
		file >> temp2;
		file >> temp;
		textures[temp2] = Render::Load(temp.c_str(), renderer);
	}
	for(int i = 0; i < gridSizeY; i++)
	{
		for(int j = 0; j < gridSizeX; j++)
		{
			file >> map[i][j];
		}
	}
	
	for(int i = 0; i < gridSizeY; i++)
	{
		for(int j = 0; j < gridSizeX; j++)
		{
			cout << map[i][j];
		}
		cout << endl;
	}
}

void Map::Render(SDL_Renderer* renderer)
{
	SDL_Rect dst, clip;
	clip.x = 0;
	clip.y = 0;
	clip.w = 24;
	clip.h = 24;
	dst.w = tileSize;
	dst.h = tileSize;
	dst.x = 0;
	dst.y = 0;
	for(int i = 0; i < gridSizeY; i++)
	{
		for(int j = 0; j < gridSizeX; j++)
		{
			dst.x = j*tileSize+pos.x;
			clip.w = Render::GetDimension( textures[ map[i][j] ] ).w;
			clip.h = Render::GetDimension( textures[ map[i][j] ] ).h;
			if(map[i][j] != 0)
				SDL_RenderCopy(renderer, textures[ map[i][j] ], &clip, &dst);
		}
		dst.y = i*tileSize+pos.y;
	}
}

void Map::SetPos(int x, int y)
{
	pos.x = x;
	pos.y = y;
}

void Map::SetPosRelative(int x, int y)
{
	pos.x += x;
	pos.y += y;
}

void Map::Loop()
{
	
}
