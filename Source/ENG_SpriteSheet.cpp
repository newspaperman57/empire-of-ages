/*
For managing a SpriteSheet in an easy way. 

Holds a texture with a SpriteSheet. Can cut the texture in a lot smaller textures, and put them in order
*/

#include "../Headers/ENG_SpriteSheet.h"

/*
Constructor. 
@param path The path to the File
@param renderer The renderer(Screen) the file is being put on. SDL_RenderCopy needs it
@param gridSize The size of each sprite in the spritesheet
*/
SpriteSheet::SpriteSheet()
{

}

SpriteSheet::~SpriteSheet()
{
	SDL_DestroyTexture(texture);
}

void SpriteSheet::Initialize(char* path, SDL_Renderer* renderer, int gridWidth, int gridHeight)
{
	flipped = false;
	angle = 0;
	center = new SDL_Point;
	
	cout << "Initializing SpriteSheet!" << endl;
	if((texture = Render::Load(path, renderer)) == NULL)
		cout << "ERROR: Spritesheet: " << SDL_GetError() << endl;
	this->gridWidth = gridWidth;
	this->gridHeight = gridHeight;
	center->x = Render::GetDimension(texture).x;
	center->y = Render::GetDimension(texture).y;
}

void SpriteSheet::Initialize(char* path, SDL_Renderer* renderer, int gridSize)
{
	Initialize(path, renderer, gridSize, gridSize);
}

void SpriteSheet::SetAngle(int newAngle)
{
	angle = newAngle;
}

void SpriteSheet::SetCenter(SDL_Point* newCenter)
{
	center = newCenter;
}

void SpriteSheet::SetFlipped(bool newFlipped)
{
	flipped = newFlipped;
}

void SpriteSheet::SetScale(double newScale)
{
	scale = newScale;
}

void SpriteSheet::Render(int x, int y, int x2, int y2, SDL_Renderer* renderer)
{
	SDL_Rect clip;
	clip.x = x*gridWidth;
	clip.y = y*gridHeight;
	clip.w = gridWidth;
	clip.h = gridHeight;
	
	SDL_Rect dst;
	dst.x = x2;
	dst.y = y2;
	dst.w = gridWidth*scale;
	dst.h = gridHeight*scale;
	
	if(!flipped)
		SDL_RenderCopyEx(renderer, texture, &clip, &dst, NULL, NULL, SDL_FLIP_NONE);
	else
		SDL_RenderCopyEx(renderer, texture, &clip, &dst, angle, center, SDL_FLIP_HORIZONTAL);
}
