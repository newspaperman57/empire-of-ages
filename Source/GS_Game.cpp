/*
GameState GAME
Holds constructor/destructor/loop/render functions and events
*/

#include "../Headers/GS_Game.h"

Character	GS_Game::character;
bool		GS_Game::D_Down;
bool 		GS_Game::A_Down;
Map 		GS_Game::map;
float 		GS_Game::speed;

GS_Game::GS_Game()
{
	speed = 5;
	D_Down = false;
	A_Down = false;
	Eventhandler::Register(GAME, SDL_MOUSEBUTTONDOWN,GS_Game::MouseButtonDown);
}

static void GS_Game::Initialize(SDL_Renderer* renderer)
{
	cout << "Initializing GS_Game!" << endl;
	map.Initialize("Maps/map1.map", renderer);
	character.Initialize(0,350, "Graphics/worker3.png", renderer, 16, 17);
}

static void GS_Game::Constructor()
{

}

static void GS_Game::Destructor()
{
	
}

static void GS_Game::MouseButtonDown(SDL_Event event)
{
	character.WorkAt(event.motion.x, event.motion.y);
}

static void GS_Game::Loop()
{
	character.Loop();
}

static void GS_Game::Render(SDL_Renderer* renderer)
{
	map.Render(renderer);
	character.Render(renderer);
}
