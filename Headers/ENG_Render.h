/*

Headerfile for "Source/Render.cpp"
Class definition for 'Render'

*/

#ifndef _RENDER_
#define _RENDER_

#include <iostream>

#include "SDL2/SDL.h"
#include "SDL2/SDL_image.h"

using namespace std;

class Render
{
	public:
		static SDL_Texture* Load(char*, SDL_Renderer*);
		static SDL_Rect GetDimension(SDL_Texture*);
};

#endif
