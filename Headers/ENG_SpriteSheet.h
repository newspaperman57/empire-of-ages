#ifndef _SPRITESHEET_
#define _SPRITESHEET_

#include <SDL2/SDL.h>
#include "ENG_Render.h"
//#include "ENG_Config.h"

class SpriteSheet
{
	private:
		SDL_Texture* texture;
		int gridHeight;
		int gridWidth;
		int angle;
		SDL_Point* center;
		bool flipped;
		double scale;
	public:
		SpriteSheet();
		~SpriteSheet();
		void Initialize(char*, SDL_Renderer*, int, int);
		void Initialize(char*, SDL_Renderer*, int);
		void Render(int, int, int, int, SDL_Renderer*);
		void SetAngle(int);
		void SetCenter(SDL_Point*);
		void SetFlipped(bool);
		void SetScale(double);
};

#endif
