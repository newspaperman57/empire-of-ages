/*

Headerfile for 'Source/Game.cpp'
Class definition for class 'Game'

*/

#ifndef _GAME_
#define _GAME_

#include <iostream>
#include <vector>
#include "SDL2/SDL.h"

#include "ENG_Render.h"
#include "ENG_Eventhandler.h"

using namespace std;

class Game
{
	private:
		SDL_Window*		window;
		SDL_Renderer*	renderer;
		SDL_Texture*	texture;
		SDL_Event 		event;
		SDL_Surface*	test_Surface;
		
		int gameState;
		
		void (*LoopFunction[64])();
		void (*RenderFunction[64])(SDL_Renderer*);
		void (*ConstructFunction[64])();
		void (*DestructFunction[64])();
		
		int newGameState;
		static bool quit;
		bool closed;
		
	public:
		Game();
		~Game();
		
		SDL_Renderer* Initialize(int, int, char*, int, int);
		static void OnQuitEvent(SDL_Event);
		bool Run(int);
		bool ChangeGamestate(int);
		void Quit();
		
		void SetConstructFunction( int gamestate, void (*)() );
		void SetLoopFunction( int gamestate, void (*)() );
		void SetRenderFunction( int gamestate, void (*)(SDL_Renderer*) );
		void SetDestructFunction( int gamestate, void (*)() );
		
		bool Close();
};

#endif
