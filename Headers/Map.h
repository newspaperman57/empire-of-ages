#ifndef _MAP_
#define _MAP_

#include <SDL2/SDL.h>
#include <fstream>
#include <string>
#include "ENG_Eventhandler.h"
#include "ENG_Render.h"
#include "ENG_Config.h"

class Map
{
	private:
		SDL_Rect pos;
		SDL_Texture* textures[20];
		int map[200][200];
		int tileSize;
		int gridSizeY, gridSizeX;
	public:
		void Initialize(char*, SDL_Renderer*);
		void Render(SDL_Renderer*);
		void SetPos(int, int);
		void SetPosRelative(int, int);
		void Loop();
};

#endif
