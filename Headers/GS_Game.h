#ifndef _GS_GAME_
#define _GS_GAME_

#include <SDL2/SDL.h>
#include "ENG_Eventhandler.h"
#include "Character.h"
#include "ENG_Config.h"
#include "Map.h"
/*
enum
{
	WALKING,
	RUNNING,
	JUMPING,
	CRAWLING
};

enum
{
	UP,
	LEFT,
	RIGHT,
	DOWN
};*/

class GS_Game
{
	private:
		static Character character;
		static bool D_Down;
		static bool A_Down;
		static Map map;
		static float speed;
	public:
		GS_Game();
		static void MouseButtonDown(SDL_Event);
		static void Initialize(SDL_Renderer*);
		static void Constructor();
		static void Loop();
		static void Render(SDL_Renderer*);
		static void Destructor();
};

#endif
