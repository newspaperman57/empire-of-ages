#ifndef _CHARACTER_
#define _CHARACTER_

#include <cmath>
#include "SDL2/SDL.h"
#include "ENG_Render.h"
#include "ENG_SpriteSheet.h"

struct Pos
{
	float x;
	float y;
};

enum
{
	STANDING,
	WALKING
};

enum
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

class Character
{
	private:
		Pos pos; // The position of the character
		Pos velo;
		Pos unitVector;
		Pos goal;SDL_Texture* texture;
		
		int animationCount, walkingDirection;
		SpriteSheet sprites;
		SDL_Point* center;
		int angle;
		double speed, magnitude, walkedDistance;
		
		double LengthOfVector(double, double);
	public:
		void WorkAt(int, int);
		void FindNextPos();
		void FindUnitVector();
		int state;
		void Initialize(SDL_Rect, const char*, SDL_Renderer*, int, int); // X, Y
		void Initialize(int, int, const char*, SDL_Renderer*, int, int); // X, Y
		void SetPos(int x, int y);
		void SetPos(SDL_Rect);
		SDL_Rect GetPos(); // Returns current position
		void SetDirection(int);
		void Move(float);
		void SetTexture( SDL_Texture ); // New Texture
		void Render( SDL_Renderer* ); // Screen
		void Loop();
		void SetState(int);
		float GetVelocityY();
		float GetVelocityX();
		void SetDirectionHorizontal(int);
		void SetVelocityX( float );
		void SetVelocityY( float );
		void SetVelocityRelative(float, float); // X, Y
};

#endif
