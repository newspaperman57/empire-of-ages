/*

Headerfile for 'Source/Eventhandler.cpp'
Class definition for 'Eventhandler'

*/

#ifndef _EVENTHANDLER_
#define _EVENTHANDLER_

#include <iostream>
#include <vector>
#include <SDL2/SDL.h>

using namespace std;

/*
Struct to avoid making 2 vectors.
@function A function registered to be called when a event of eventType gets polled
@eventType When this event occurres, function will be called
*/
struct registeredFunction
{
	void (*function)(SDL_Event);
	int eventType;
	int gameState;
};

class Eventhandler
{
	private:
		static SDL_Event event;
		static std::vector<registeredFunction> functionRegister;
	
	public:
		static bool Initialize();
		static void Register(int, int, void (*)(SDL_Event));
		static void ProcessEvents(int);
};

#endif
