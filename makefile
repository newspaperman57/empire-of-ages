all: main

main: main.cpp ObjectFiles/ENG_Game.o ObjectFiles/ENG_Render.o ObjectFiles/ENG_Eventhandler.o ObjectFiles/GS_Game.o ObjectFiles/Character.o ObjectFiles/ENG_SpriteSheet.o ObjectFiles/Map.o
	g++ main.cpp ObjectFiles/ENG_Game.o ObjectFiles/ENG_Render.o ObjectFiles/ENG_Eventhandler.o ObjectFiles/GS_Game.o ObjectFiles/Character.o ObjectFiles/ENG_SpriteSheet.o ObjectFiles/Map.o -o main -lSDL2 -lSDL2_image -Wl,-rpath,/usr/local/lib --debug

ObjectFiles/ENG_Game.o: Headers/ENG_Game.h Source/ENG_Game.cpp
	g++ -c Source/ENG_Game.cpp -o ObjectFiles/ENG_Game.o -fpermissive -w

ObjectFiles/ENG_Render.o: Headers/ENG_Render.h Source/ENG_Render.cpp
	g++ -c Source/ENG_Render.cpp -o ObjectFiles/ENG_Render.o -fpermissive -w

ObjectFiles/ENG_Eventhandler.o: Headers/ENG_Eventhandler.h Source/ENG_Eventhandler.cpp
	g++ -c Source/ENG_Eventhandler.cpp -o ObjectFiles/ENG_Eventhandler.o -fpermissive -w

ObjectFiles/GS_Game.o: Headers/GS_Game.h Source/GS_Game.cpp
	g++ -c Source/GS_Game.cpp -o ObjectFiles/GS_Game.o -fpermissive -w
	
ObjectFiles/Character.o: Headers/Character.h Source/Character.cpp
	g++ -c Source/Character.cpp -o ObjectFiles/Character.o -fpermissive -w
	
ObjectFiles/ENG_SpriteSheet.o: Headers/ENG_SpriteSheet.h Source/ENG_SpriteSheet.cpp
	g++ -c Source/ENG_SpriteSheet.cpp -o ObjectFiles/ENG_SpriteSheet.o -fpermissive -w

ObjectFiles/Map.o: Headers/Map.h Source/Map.cpp
	g++ -c Source/Map.cpp -o ObjectFiles/Map.o -fpermissive -w

clean:
	rm ObjectFiles/*.o main
